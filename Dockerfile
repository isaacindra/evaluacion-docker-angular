
FROM node:alpine as build
WORKDIR /usr/app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build --prod


FROM nginx
EXPOSE 80
COPY --from=build /usr/app/dist/evaluacionAngular /usr/share/nginx/html